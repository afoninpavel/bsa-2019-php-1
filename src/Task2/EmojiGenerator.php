<?php

declare(strict_types=1);

namespace App\Task2;

class EmojiGenerator
{
    private $items = ['🚀', '🚃', '🚄', '🚅', '🚇'];

    public function generate(): \Generator
    {
        yield from $this->items;
    }
}
