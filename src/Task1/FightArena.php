<?php

declare(strict_types=1);

namespace App\Task1;

class FightArena
{
    private $arena;

    public function add(Fighter $fighter): void
    {
        $this->arena[] = $fighter;
    }

    public function mostPowerful(): Fighter
    {
        $maxAttack = 0;
        $key = null;

        foreach ($this->arena as $k => $fighter) {
            if ($fighter->getAttack() > $maxAttack) {
                $maxAttack = $fighter->getAttack();
                $key = $k;
            }
        }
        return $this->arena[$key];
    }

    public function mostHealthy(): Fighter
    {
        $maxHealth = 0;
        $key = null;

        foreach ($this->arena as $k => $fighter) {
            if ($fighter->getHealth() > $maxHealth) {
                $maxHealth = $fighter->getHealth();
                $key = $k;
            }
        }
        return $this->arena[$key];
    }

    public function all(): array
    {
        return $this->arena;
    }
}
