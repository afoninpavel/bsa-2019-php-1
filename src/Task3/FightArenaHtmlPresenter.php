<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\FightArena;

class FightArenaHtmlPresenter
{
    public function present(FightArena $arena): string
    {
        $fighters = $arena->all();
        $http = '';
        foreach ($fighters as $fighter) {
            $http .= "<img src=\"{$fighter->getImage()}\">{$fighter->getName()}: {$fighter->getAttack()}, {$fighter->getHealth()}";
        }
        return $http;
    }
}
